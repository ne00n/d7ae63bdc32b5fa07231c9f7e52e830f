import requests, random, os

reader = open('proxies.txt', 'r')
proxies = reader.readlines()
proxiesList = []

for proxy in proxies:
    proxy = proxy.replace("\n","")
    try:
        r = requests.head(f"http://10.0.{proxy}.1:8888", timeout=(5, 5))
        print(f"{proxy } is reachable")
        proxiesList.append(proxy)
    except:
        print(f"{proxy } failed to connect")
        
print(f"Got {len(proxiesList)} working proxies")

reader = open('channels.txt', 'r')
Lines = reader.readlines()

for line in Lines:
    line = line.replace("\n","")
    splitted = line.split("/")
    last = splitted[len(splitted) -1].replace("@","")
    print(f"Running {last}")
    os.makedirs(last, exist_ok=True) 
    os.chdir(last)
    proxy = random.choice(proxiesList)
    os.system(f"yt-dlp --proxy http://10.0.{proxy}.1:8888 --download-archive history.txt --embed-thumbnail -f bestaudio -x --audio-quality 320k https://www.youtube.com/@{last}")
    os.chdir("..")